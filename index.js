// console.log("Hello World");


let trainer = {
    name: 'Nilly',
    age: 28,
    pokemon: ['Blaziken', 'Mightyena', 'Glaceon', 'Gardevoir'],
    friends: {
    	kanto: ['Ash','Misty','Brock'],
    	hoenn: ['May','Max']
	}
};

console.log(trainer);

// dot notation
console.log('Result from dot notation:');
console.log(trainer.name);

// square bracket notation
console.log('Result from square bracket notation:');
console.log(trainer['pokemon']);

// talk method
console.log('Result of talk method');
trainer.talk = function() {
	console.log('Pikachu! I choose you!');
};

trainer.talk();

function Pokemon(name, level){
    // Properties
    this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods

	this.faint = function() {
		console.log(this.name + ' fainted.')
	}

	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		target.health -= this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);
		if(target.health <= 0) {
			// console.log(target.name + ' fainted.')
			this.faint()
		}
	}

}

let vulpix = new Pokemon('Vulpix', 10);
console.log(vulpix);

let sandshrew = new Pokemon('Sandshrew', 8);
console.log(sandshrew);

let entei = new Pokemon('Entei', 100);
console.log(entei);

vulpix.tackle(sandshrew);
console.log(sandshrew);

entei.tackle(vulpix);
console.log(vulpix);

